jQuery(document).ready(function($){

  var words = $(".animWord").data("words");
  var locations = $(".homeCTA span").data("words");
  var content = $(".hero-content").data("content");

  if ($("#homeHero").data("transition")) {
    var transition = 'slide';
  } else {
    var transition = 'fade';
  }

  $('.animMargin').css('margin-left', $('.inner>h1:eq(1)>span').width() + 5);

  setTimeout(function(){
    $('.animMargin').removeClass('notransition');
  }, 1);

  var speed = 1500;

  $("#homeHero").okCycle({
    autoplay: true,
    transition: 'fade',
    duration: parseInt($("#homeHero").data("duration")),
    speed: parseInt($("#homeHero").data("speed")),
    beforeMove: function(){
      slideContentChange();
    }
  });

  function slideContentChange(){
    animOut = function() {
      $('.inner>h1:eq(1)').css({
        width: $('.inner>h1:eq(1)').width()
      });
      // Word Speed out
      $(".inner>h1:eq(1)>span").transition({ opacity: 0, translate: [0,15] }, parseInt($("#homeHero").data("speedout")), function() {
        contentSwap();
      });
    };
    contentSwap = function(){
      // Word Delay
      $(".inner>h1:eq(1)>span").transition({ opacity: 0, translate: [0,-15] }, parseInt($("#homeHero").data("delay")), function() {
        animIn();
      });
      var index = $('.active').index();
      $('.inner>h1:eq(1)>span').css({
        backgroundColor: words[index][1]
      });
      $('.inner>h1:eq(1)>span').html('' + words[index][0] + '');
      $('.hero-content').html(content[index]);
      $('.animMargin').css('margin-left', $('.inner>h1:eq(1)>span').width()+5);

      if (typeof locations != 'undefined' && locations.length > 0) {
        $('.homeCTA>p>span').html(''+locations[index][0]+'');
      };
    };
    animIn = function(){
      // Word Speed In
      $(".inner>h1:eq(1)>span").transition({ opacity: 1, translate: [0,0] }, parseInt($("#homeHero").data("speedin")));
    };
    animOut();

  };
});
