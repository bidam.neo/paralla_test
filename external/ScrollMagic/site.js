(function($){
  var Site = {
    init: function(){
      if ($("#blog").length) return;

      window.scrollTo(0,0);

      controller = new ScrollMagic();

      $('.parallax-scroll').parallaxBackgroundFixed();

      Site.lazyLoad();
      Site.sizing();
      Site.logoTween();

      if ( $('#hero').length ) { Site.hero(); }
      if ( $('#lifestyle').length ) { Site.lifestyle(); }
      if ( $('#showcase').length ) { Site.showcase(); }
      if ( $('#demo').length ) { Site.demo(); }
      if ( $('#jobs-container').length ) { Site.jobs(); }
      if ( $('body.home').length ) { Site.navigation(); }
    },
    jobs: function(){
      var container = '#jobs-container';
      var jobviteAPI = "https://api.jobvite.com/v1/jobFeed?companyId=qob9Vfwn&api=hoteltonight_api_key&sc=5ebd620a47f226271e3b7826b4be497a&callback=?";
      $.getJSON( jobviteAPI, {
        format: "json"
      }).done(function( data ) {

        function sortByCategory(a, b) {
          var aCat = a.category.toLowerCase();
          var bCat = b.category.toLowerCase();
          return ((aCat < bCat) ? -1 : ((aCat > bCat) ? 1 : 0));
        }

        function getAllJobs() {
          var jobs = data.jobs.sort(sortByCategory);

          for (var n = 0; n < data.jobs.length; n++) {
            var category = data.jobs[n].category;
            var previous_cateogy = n > 0 ? data.jobs[n-1].category : '';
            var div = $("<div class='job'>");

            if ( category !== previous_cateogy ) {
              $("<h3 style='font-weight: bold;'>"+category+"</h3>").appendTo(div);
            }

            $("<p><a href='/jobs/?jobId="+data.jobs[n].id+"'>" + data.jobs[n].title + "</a></p>").appendTo(div);

            div.appendTo(container);
          }
        }

        function getSingleJob(jobId) {
          var div = $('#content-inner'),
            header = $('#content-header .inner'),
            title = '',
            html = '';
          var jobs = $.grep(data.jobs, function(job){ return job.id == jobId; });
          if (jobs.length != 0) {
            var job = jobs[0];
            html += '<div class="row">'
            html += '<h3>' + job.title + '</h3>'
            html += '<p>'+ job.description+'</p>';
            html += '<a class="button large primary" target="_blank" href="'+job.applyUrl+'">Apply Now</a>';
            html += '</div>';
            div.html(html);
            title += '<h1>' + job.title + '</h1>';
            title += '<h3>' + job.category + ' | ' + job.location + '</h3>';
            header.html(title);
          }
        }

        function getParameterByName( name ){
          name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
          var regexS = "[\\?&]"+name+"=([^&#]*)",
              regex = new RegExp( regexS ),
              results = regex.exec( window.location.href );
          if( results == null ){
            return "";
          } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
          }
        }

        var jobId = getParameterByName('jobId');

        if ( jobId ) {
          getSingleJob(jobId);
        } else {
          getAllJobs();
        }

        $('.jobs-content').slideUp(0);
        $(document).on('click', '.jobs-trigger', function(){
          var next = $(this).next('.jobs-content');
          $('.jobs-content').not(next).slideUp();
          next.slideDown();
        });
      });
    },
    lazyLoad: function() {
      var images = $('.lazy');
      $.each(images, function(index, image){
        var image = $(image);
        var background = image.data('background');
        var original = image.data('original');

        if ( background ) {
          image.css({ backgroundImage: 'url('+background+')'});
        } else if ( original ) {
          image.attr('src', original);
        }
      });
    },
    navigation: function(){

      var nav = $('<ul>').attr('id', 'parallax-nav');
      var sections = $('section[data-title]');

      sections.each(function(index, section){
        var title = $(section).data('title');
        nav.append('<li><a href="#'+section.id+'"><span>'+title+'</span></a></li>');
      });

      nav.appendTo('body');

      $('#parallax-nav li').first().addClass('active');

      // vertical center the nav
      var windowHeight = $(window).height();
      var navHeight = $('#parallax-nav').height();
      var top = (windowHeight - navHeight)/2;

      $('#parallax-nav').css({ top: top });

      $(document).on('click', '#parallax-nav li a', function(event){
        event.preventDefault();
        var href = $(this).attr('href');
        var offset = ( $(window).height() / 2 );
        if ( href == '#hero' ) { offset = 0; }
        if ( href == '#apps' ) { Site.updateActiveNav('apps'); }
        $('body, html').scrollTop( $(href).offset().top + offset );
      });

      // add active class to last menu item when you scroll to the bottom of the page
      $(window).scroll(function() {
        if( $(window).scrollTop() + $(window).height() == $(document).height() ) {
          Site.updateActiveNav('apps');
        }
      });
    },
    updateActiveNav: function(section, direction) {
      $('#parallax-nav li').removeClass('active');
      $('#parallax-nav').find('a[href="#'+section+'"]').parents('li').addClass('active');
    },
    sizing: function() {
      if ( $('body').hasClass('home') ) {
        var apps = $("#apps");
        var appsHeight = $("#apps").height();
        apps.css({ marginTop: -appsHeight });
      }

      $('#demo .background').height( $(window).height() );
    },
    logoTween: function(){
      var logoTween1 = new TimelineMax().add([ TweenMax.to( $('.logo img.primary'), 1, { height: 30, opacity: 0, ease: Linear.easeOut } ) ]);
      var scene = new ScrollScene({ duration: 500, offset: 1 })
          .addTo(controller)
          .setTween(logoTween1);

      var logoTween2 = new TimelineMax().add([ TweenMax.to( $('.logo img.secondary'), 1, { height: 30, opacity: 1, ease: Linear.easeOut } ) ]);
      var scene = new ScrollScene({ duration: 500, offset: 1 })
          .addTo(controller)
          .setTween(logoTween2);
    },
    hero: function() {
      $(document).on('click', '.icon-arrow-down', function(e){
        e.preventDefault();
        $('body, html').animate({
          scrollTop: $(this.hash).offset().top
        }, 500);
      });
    },
    // ----------------------------
    //
    // CASE STUDIES
    //
    // ----------------------------
    lifestyle: function(){
      var windowHeight = $(window).height(),
          slides = $('#lifestyle .slide'),
          numSlides = slides.length,
          offset = $('#lifestyle').offset().top;

      $('#lifestyle').find('.slide').height( windowHeight );

      var scene = new ScrollScene({
          triggerElement: "#lifestyle"
        , triggerHook: 0
        , duration: windowHeight*(numSlides)
        })
        .setPin("#lifestyleSlider")
        .addTo(controller)
        .on('enter', function(event){
          Site.updateActiveNav('lifestyle', event.scrollDirection);
        })
        .on('leave', function(event){
          if ( event.scrollDirection === 'REVERSE' ) {
            Site.updateActiveNav('hero', event.scrollDirection);
          }
        });

      $.each(slides, function(index, slide) {
        var slide = $(slide);
        var slideLeft = $(slide).find('.slide-left');
        var slideRight = $(slide).find('.slide-right');
        var text = $(slide).find('h1');
        var subtext = $(slide).find('p');

        var opacity = index == 0 ? 1 : 0; // don't fade in the first slide

        var tween = new TimelineMax()
          .add([
            TweenMax.fromTo( slide, 0.25, { opacity: opacity }, { opacity: 1 })
          , TweenMax.fromTo( text, 2, { marginTop: (windowHeight*0.7)+'px' }, {marginTop: 0, ease: Linear.easeNone })
          , TweenMax.fromTo( text, 0.5, { opacity: 0 }, { opacity: 1, ease:Linear.easeNone, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })
          , TweenMax.fromTo( subtext, 1, { marginTop: '100px' }, {marginTop: 0, ease: Linear.easeNone })
          , TweenMax.fromTo( subtext, 0.5, { opacity: 0 }, { opacity: 1, ease:Linear.easeNone, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })
          ]);

        var scene = new ScrollScene({
          duration: windowHeight
        , offset: windowHeight*(index+1)
        })
        .addTo(controller)
        .setTween(tween);

      });
    },
    // ----------------------------
    //
    // HOW IT WORKS
    //
    // ----------------------------
    demo: function(){
      var windowHeight = $(window).height();
      var slides = $('#demo .slide');
      var offset = $('#demo').offset().top;
      var numSlides = slides.length;

      slides.height( windowHeight );

      var scene = new ScrollScene({
          triggerElement: "#demo"
          , triggerHook: 0
          , duration: windowHeight*(numSlides-1)
        })
        .setPin("#demoSlider")
        .addTo(controller)
        .on('enter', function(event){
          Site.updateActiveNav('demo', event.scrollDirection);
          $('#animation-1').get(0).play();
        });

      $('#demo').css({
        marginTop: windowHeight,
        marginBottom: -windowHeight,
        minHeight: windowHeight*(numSlides)
      }); // hackity hack to fix pin spacer height

      // let's position the iphone using jquery
      // $('.iphone-wrapper img').one('load', function(){
        var phone = $('.iphone-wrapper img');
        // var phoneHeight = phone.height();
        // var phoneWidth = phone.width();
        var phoneHeight = 634;
        var phoneWidth = 310;

        $('.iphone-wrapper').css({
          left: ($(window).width()/2) + (( $(window).width()/2 ) - phoneWidth)/2,
          top: ($(window).height() - phoneHeight)/2
        });
      // });

      $.each(slides, function(index, slide) {
        var slide = $(slide);
        var text = slide.find('h2');
        var subtext = slide.find('p');

        var opacity = index == 0 ? 1 : 0; // don't fade in the first slide

        var tween = new TimelineMax()
          .add([
              TweenMax.fromTo( slide, 0.25, { opacity: opacity }, { opacity: 1 })
            , TweenMax.fromTo( text, 2, { marginTop: (windowHeight*0.7)+'px' }, {marginTop: 0, ease: Linear.easeNone })
            , TweenMax.fromTo( text, 0.5, { opacity: 0 }, { opacity: 1, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })
            , TweenMax.fromTo( subtext, 1, { marginTop: '100px' }, {marginTop: 0, ease: Linear.easeNone })
            , TweenMax.fromTo( subtext, 0.5, { opacity: 0 }, { opacity: 1, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })
          ]);

        var scene = new ScrollScene({
          duration: windowHeight
          , offset: offset + windowHeight*(index+0.5)
        })
        .addTo(controller)
        .setTween(tween);

        var sceneOut = new ScrollScene({
          triggerHook: 'onLeave'
        })
        .addTo(controller)
        .on('enter', function(event){
          var video = $('#animation-'+(index+2));
          if (video.length) {
            video.get(0).play();
          }
        });
      });
    },
    // ----------------------------
    //
    // HOTELS
    //
    // ----------------------------
    showcase: function(){
      var windowHeight = $(window).height();
      var slides = $('#showcase .slide');
      var offset = $('#showcase').offset().top;
      var numSlides = slides.length;

      // hackity hack:
      $('#showcase').css({ marginTop: windowHeight });

      slides.height( windowHeight );

      var scene = new ScrollScene({
          triggerElement: "#showcase"
          , triggerHook: 0
          , duration: windowHeight*(numSlides) + (windowHeight/2)
          , offset: 1
        })
        .setPin("#showcaseSlider")
        .addTo(controller)
        .on('enter', function(event){
          Site.updateActiveNav('showcase', event.scrollDirection);
        });


      $.each(slides, function(index, slide) {
        var slide = $(slide);
        var text = $(slide).find('.caption-inner');
        var subtext = $(slide).find('p');
        var opacity = index == 0 ? 1 : 0; // don't fade in the first slide

        var tween = new TimelineMax()
          .add([
              TweenMax.fromTo( slide, 0.25, { opacity: opacity }, { opacity: 1 })
            , TweenMax.fromTo( text, 2, { marginTop: (windowHeight*0.7)+'px' }, {marginTop: 0, ease: Linear.easeNone })
            , TweenMax.fromTo( text, 0.5, { opacity: 0 }, { opacity: 1, ease:Linear.easeNone, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })
            , TweenMax.fromTo( subtext, 1, { marginTop: '100px' }, {marginTop: 0, ease: Linear.easeNone })
            , TweenMax.fromTo( subtext, 0.5, { opacity: 0 }, { opacity: 1, ease:Linear.easeNone, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })

          ]);

        var scene = new ScrollScene({
          duration: windowHeight
          // WTF: Changing the number from 5 to 4 allows more hotels to show up on the homepage
        , offset: offset + windowHeight*(index+4)
        })
        .addTo(controller)
        .setTween(tween);

      });
    }
  };

  var Mobile = {
    init: function(){
      Mobile.sizing();
      Mobile.lazyLoad();
      Mobile.menu();
      Site.jobs();

      if ( $('body.home').length ) { Mobile.detectDevice(); }

      $('#showcaseSlider').slick({
        pauseOnHover: false,
        autoplay: true,
        dots: true,
        arrows: false
      });

      $('#lifestyleSlider').slick({
        pauseOnHover: false,
        autoplay: true,
        dots: true,
        arrows: false
      }).find('h1').css({
        marginTop: ($(window).height()/2) - 150
      });
    },
    detectDevice: function() {
      var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i);
          }
      };

      if ( isMobile.Android() ) {
        $('.mobileCTA > a').not('.android').hide();
      }

      if ( isMobile.iOS() ) {
        $('.mobileCTA > a').not('.ios').hide();
      }

      if ( isMobile.Windows() ) {
        $('.mobileCTA > a').not('.windows').hide();
      }

    },
    menu: function(){
      var header = $('#header');
      var nav = $('#header .nav');

      nav.slideUp(0);

      $('.hamburger').click(function(){
        if ( header.hasClass('active') ) {
          nav.slideUp('slow');
          header.removeClass('active');
        } else {
          nav.slideDown('slow');
          header.addClass('active');
        }

      });

    },
    lazyLoad: function() {
      var images = $('.lazy-mobile');

      $.each(images, function(index, image){
        var image = $(image);
        var background = image.data('background');
        var original = image.data('original');

        if ( background ) {
          image.css({ backgroundImage: 'url('+background+')'});
        } else if ( original ) {
          image.attr('src', original);
        }
      });
    },
    sizing: function(){

      var windowHeight = $(window).height();
      var sections = $(' #lifestyle');
      sections.not('#lifestyle').css({ 'min-height': windowHeight });

      $('#hero').height( windowHeight );

    }
  }

  var mobileBorder = 1024;
  $(document).ready(function(){
    has3dTransform = has3d();
    if ( $(window).width() > mobileBorder ) {
      Site.init();
    } else {
      Mobile.init();
    }
  });

  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  var width = $(window).width();
  $(window).bind('resize', function(){
    if($(window).width() !== width){
      delay(function(){
        if ((width > mobileBorder && $(window).width() < mobileBorder)
          || (width < mobileBorder && $(window).width() > mobileBorder)){
          location.reload();
        }

      }, 500);
    }
  });

  $(document).on("click", ".logo", function(){
    $('html, body').animate({ scrollTop: 0}, "slow");
  });

  $.fn.extend({
    parallaxBackgroundFixed: function () {
      var self = this;
      var _resize = function() {
        self.each(function() {
          var parallax = $(this);
          var cssObj = {}
          if(has3dTransform){
            cssObj = {
              'webkitTransform':'translate3d( 0,'+( - ( ( parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'OTransform':'translate3d( 0,'+( - ( ( parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'msTransform':'translate3d( 0,'+( - ( (parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'MozTransform':'translate3d( 0,'+( - ( ( parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'transform':'translate3d( 0,'+( - ( ( parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
            }
          }
          else
            cssObj={"background-position": '50%' + ( - ( ( parallax.offset().top - $(window).scrollTop() ) * .7 ) )+"px"}


          parallax.find('.parallax-image').css(cssObj);
        })
      };

      _resize();

      var _scroll = function(e) {

      var cssObj = {}
        self.each(function() {
          if(has3dTransform){
            cssObj = {
              'webkitTransform':'translate3d( 0,'+( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'OTransform':'translate3d( 0,'+( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'msTransform':'translate3d( 0,'+( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'MozTransform':'translate3d( 0,'+( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
              ,'transform':'translate3d( 0,'+( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px , 0)"
            }
          }
          else
            cssObj={"background-position": '50%' + ( - ( ( $(this).offset().top - $(window).scrollTop() ) * .7 ) )+"px"}
                  $(this).find('.parallax-image').css(cssObj);
              });
        };

      $(document).scroll( _scroll );
      $(window).resize( _resize );

      return this;
    }
  });

  function has3d() {
      var el = document.createElement('p'),
          has3d,
          transforms = {
              'webkitTransform':'-webkit-transform',
              'OTransform':'-o-transform',
              'msTransform':'-ms-transform',
              'MozTransform':'-moz-transform',
              'transform':'transform'
          };

      // Add it to the body to get the computed style.
      document.body.insertBefore(el, null);

      for (var t in transforms) {
          if (el.style[t] !== undefined) {
              el.style[t] = "translate3d(1px,1px,1px)";
              has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
          }
      }

      document.body.removeChild(el);

      return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
  }
})(jQuery);
